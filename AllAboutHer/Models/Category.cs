﻿//
//        XXXXXXXXXXXX            AllAboutHer                   BBBBBB
//       XX          XX                                         BB   BB
//      XX  XX    XX  XX          Raleigh Mobile .NET           BBBBBB
//     XX    XX  XX    XX         (Xamarin) Developers Group    BB   BB SSSSSS
//    XX      XXXX      XX                                      BBBBBB SS
//   XX        XX        XX       © 2018 Bobby Skinner                  SSSSS
//    XX      XXXX      XX        All rights reserved.                      SS
//     XX    XX  XX    XX                                              SSSSSS
//      XX  XX    XX  XX          BSD License
//       XX          XX
//        XXXXXXXXXXXX
//
//  Redistribution and use in source and binary forms are permitted
//  provided that the above copyright notice and this paragraph are
//  duplicated in all such forms and that any documentation,
//  advertising materials, and other materials related to such
//  distribution and use acknowledge that the software was developed
//  by Bobby Skinner. The name of Bobby Skinner may not be used to endorse 
//  or promote products derived from this software without specific prior 
//  written permission.
//
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//  Note: This license was specifically chosen to be incompatible with
//        the GPL license.
using System;
using SQLite;

namespace AllAboutHer.Models
{
    public class Category : IDatabaseObject
    {
        /// <summary>
        /// This is the key fo be used in forming relationships and identifying 
        /// the object. It must be unique and will likely be a UUID string.
        /// </summary>
        /// <value>The identifier.</value>
        [PrimaryKey]
        public String CatagoryKey { get; set; }

        /// <summary>
        /// The name of the Catagory
        /// </summary>
        /// <value>The name.</value>
        public String Name { get; set; }

        /// <summary>
        /// A detailed description of the item
        /// </summary>
        /// <value>The details.</value>
        public String Details { get; set; } = "";

        public Category()
        {
        }

        public string PrimaryKey()
        {
            return CatagoryKey;
        }

        public void SetPrimaryKey(string key)
        {
            CatagoryKey = key;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
