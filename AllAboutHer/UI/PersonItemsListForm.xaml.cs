﻿//
//        XXXXXXXXXXXX            AllAboutHer                   BBBBBB
//       XX          XX                                         BB   BB
//      XX  XX    XX  XX          Raleigh Mobile .NET           BBBBBB
//     XX    XX  XX    XX         (Xamarin) Developers Group    BB   BB SSSSSS
//    XX      XXXX      XX                                      BBBBBB SS
//   XX        XX        XX       © 2018 Bobby Skinner                  SSSSS
//    XX      XXXX      XX        All rights reserved.                      SS
//     XX    XX  XX    XX                                              SSSSSS
//      XX  XX    XX  XX          BSD License
//       XX          XX
//        XXXXXXXXXXXX
//
//  Redistribution and use in source and binary forms are permitted
//  provided that the above copyright notice and this paragraph are
//  duplicated in all such forms and that any documentation,
//  advertising materials, and other materials related to such
//  distribution and use acknowledge that the software was developed
//  by Bobby Skinner. The name of Bobby Skinner may not be used to endorse 
//  or promote products derived from this software without specific prior 
//  written permission.
//
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//  Note: This license was specifically chosen to be incompatible with
//        the GPL license.
using System;
using System.Collections.Generic;
using AllAboutHer.Models;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace AllAboutHer.UI
{
    public partial class PersonItemsListForm : ContentPage
    {
        private Person _currentPerson = null;
        public Person CurrentPerson 
        {
            get 
            {
                return _currentPerson;
            }

            set 
            {
                _currentPerson = value;

                //LoadData();
            }
        }
        public ObservableCollection<Item> Items { get; set; } = new ObservableCollection<Item>();

        public PersonItemsListForm()
        {
            InitializeComponent();

            // Create Add toolbar button
            ToolbarItem toolBarButton = new ToolbarItem("Add", null, () => { });
            ToolbarItems.Add(toolBarButton);
            toolBarButton.Clicked += (sender, args) =>
            {
                EditItemForm page = new EditItemForm();
                page.CurrentPerson = this.CurrentPerson;
                Navigation.PushAsync(page);
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await LoadData();
        }

        public async Task<ResultSet<Item>> LoadData()
        {
            ResultSet<Item> result = await Database.Current.GetItems( CurrentPerson );
            Items = new ObservableCollection<Item>(result.Data);
            ItemList.ItemsSource = Items;

            return result;
        }

        protected void OnViewCellTapped(object sender, EventArgs e)
        {

            Console.WriteLine("Item Cell pressed: {0}", ItemList.SelectedItem);

            EditItemForm page = new EditItemForm();
            page.CurrentPerson = this.CurrentPerson;
            page.CurrentItem = ItemList.SelectedItem as Item;
            Navigation.PushAsync(page);
        }

    }
}
