﻿//
//        XXXXXXXXXXXX            AllAboutHer                   BBBBBB
//       XX          XX                                         BB   BB
//      XX  XX    XX  XX          Raleigh Mobile .NET           BBBBBB
//     XX    XX  XX    XX         (Xamarin) Developers Group    BB   BB SSSSSS
//    XX      XXXX      XX                                      BBBBBB SS
//   XX        XX        XX       © 2018 Bobby Skinner                  SSSSS
//    XX      XXXX      XX        All rights reserved.                      SS
//     XX    XX  XX    XX                                              SSSSSS
//      XX  XX    XX  XX          BSD License
//       XX          XX
//        XXXXXXXXXXXX
//
//  Redistribution and use in source and binary forms are permitted
//  provided that the above copyright notice and this paragraph are
//  duplicated in all such forms and that any documentation,
//  advertising materials, and other materials related to such
//  distribution and use acknowledge that the software was developed
//  by Bobby Skinner. The name of Bobby Skinner may not be used to endorse 
//  or promote products derived from this software without specific prior 
//  written permission.
//
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
//  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
//  Note: This license was specifically chosen to be incompatible with
//        the GPL license.
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AllAboutHer.ViewModels;
using Xamarin.Forms;

namespace AllAboutHer.UI
{
    public partial class EditPersonForm : ContentPage
    {
        private EditPersonViewModel _person = null;

        public EditPersonViewModel person
        {
            get
            {
                if (_person == null)
                {
                    _person = new EditPersonViewModel();
                    _person.Birthday = DateTime.Now;
                    this.BindingContext = _person;
                }

                _person.Name = Name.Text;
                _person.Birthday = BirthdayPicker.Date;

                return _person;
            }

            set
            {
                _person = value;

                this.BindingContext = _person;

                //if (_person != null)
                //{
                //    Name.Text = _person.Name;
                //    BirthdayPicker.Date = _person.Birthday;
                //}
            }
        }

        public EditPersonForm()
        {
            InitializeComponent();

            this.BindingContext = person;

            // Create Save toolbar button
            ToolbarItem toolBarButton = new ToolbarItem("Save", null, () => { });
            ToolbarItems.Add(toolBarButton);
            toolBarButton.Clicked += SaveButtonPressed;

            // Create Items toolbar button
            ToolbarItem itemButton = new ToolbarItem("Items", null, () => { });
            ToolbarItems.Add(itemButton);
            itemButton.Clicked += (sender, args) =>
            {
                PersonItemsListForm page = new PersonItemsListForm();
                page.CurrentPerson = this.person.ModelObject;
                Navigation.PushAsync(page);
            };
        }


        public async void SaveButtonPressed(object sender, EventArgs args)
        {
            (bool successful, string error) result = await person.Save();

            if ( result.successful )
                await Navigation.PopAsync();
            else
            {
                await DisplayAlert( "Error", "Could not add that person: " + result.error, "OK" );

            }
        }
    }
}
