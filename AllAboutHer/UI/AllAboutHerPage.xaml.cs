﻿using AllAboutHer.Models;
using Xamarin.Forms;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AllAboutHer.UI;

namespace AllAboutHer
{
    public partial class AllAboutHerPage : ContentPage
    {
        Database Data = new Database();

        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await CreateDataTest();
            await LoadData();
        }

        public async Task<ResultSet<Person>> LoadData()
        {
            ResultSet<Person> result = await Database.Current.GetPeople();
            People = new ObservableCollection<Person>(result.Data);
            UserList.ItemsSource = People;

            return result;
        }

        public AllAboutHerPage()
        {
            InitializeComponent();

            // Create Add toolbar button
            ToolbarItem toolBarButton = new ToolbarItem("Add", null, () => { });
            ToolbarItems.Add(toolBarButton);
            toolBarButton.Clicked += (sender, args) =>
            {
                EditPersonForm page = new EditPersonForm();
                Navigation.PushAsync(page);
            };
        }

        protected void OnViewCellTapped(object sender, EventArgs e)
        {

            Console.WriteLine("Person Cell pressed: {0}", UserList.SelectedItem);

            EditPersonForm page = new EditPersonForm();
            page.person = new ViewModels.EditPersonViewModel( UserList.SelectedItem as Person );
            Navigation.PushAsync(page);
        }

        protected async Task CreateDataTest()
        {

            await Data.Open("AllAboutHer.sqlite");

            await Data.SaveAsync<Category>( new Category() 
            { 
                CatagoryKey="Catagory-1", 
                Name="Ring"
            } );

            await Data.SaveAsync<Category>(new Category()
            {
                CatagoryKey = "Catagory-2",
                Name = "Dress"
            });

            await Data.SaveAsync<Category>(new Category()
            {
                CatagoryKey = "Catagory-3",
                Name = "Shoe"
            });

            await Data.SaveAsync<Category>(new Category()
            {
                CatagoryKey = "Catagory-4",
                Name = "Shirt"
            });

            await Data.SaveAsync<Category>(new Category()
            {
                CatagoryKey = "Catagory-5",
                Name = "Pants"
            });

            ResultSet<Category> catagories = await Data.GetCatagories();

            if (catagories.Error != null)
                Console.WriteLine("Error: {0}", catagories.Error );
            else 
            {
                foreach( Category cat in catagories )
                {
                    Console.WriteLine( "Catagory: {0}", cat );
                }
            }
        }
    }
}
